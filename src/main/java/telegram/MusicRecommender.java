package telegram;

import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import org.apache.log4j.Logger;
import state.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public final class MusicRecommender implements UpdatesListener {
    private static Logger logger = Logger.getLogger(MusicRecommender.class);
    private static final String botToken = "389197653:AAFqJPbV2iLYVZNNJx-AOJot2kclcUmwUmc";

    private TelegramBot bot;
    private Map<Long, State> userState;

    private ThreadPoolExecutor pool;

    public MusicRecommender() {
        bot = TelegramBotAdapter.build(botToken);
        bot.setUpdatesListener(this);

        userState = new HashMap<>();

        BlockingQueue<Runnable> workQueue = new SynchronousQueue<Runnable>();
        pool = new ThreadPoolExecutor(10, 10, 120, TimeUnit.SECONDS, workQueue);
    }

    @SuppressWarnings("unchecked")
    @Override
    public int process(List<Update> list) {
        List<State> states = new ArrayList<>();

        for (Update update : list) {
            pool.execute(() -> {
                synchronized (update.message().chat().id()) {
                    logger.info("Processing...");

                    states.clear();
                    State state;

                    String message = update.message().text();

                    // Retrieve user state
                    if (!message.equals("/start") && userState.containsKey(update.message().chat().id())) {
                        state = userState.get(update.message().chat().id());
                        logger.info(String.format("User state found: %s", state.getClass().getName()));
                    } else {
                        state = new InitState();
                        userState.put(update.message().chat().id(), state);
                    }

                    logger.info(String.format("Update received: %s, User (%d, %s)",
                            update.message().text(), update.message().chat().id(), state.getClass().getName()));

                    do {
                        state = state.nextState(message);
                        states.add(state);
                    } while (state.isNextStateForceExecute());

                    for (int index = 0; index < states.size(); index++) {
                        state = states.get(index);
                        state.execute(update.message().chat().id(), message, bot);
                    }

                    userState.put(update.message().chat().id(), state);
                }
            });
        }

        return UpdatesListener.CONFIRMED_UPDATES_ALL;
    }
}
