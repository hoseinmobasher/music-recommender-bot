package state;

import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import utils.DictionaryUtils;
import utils.Key;

public class SearchState extends State {
    private SearchType type = SearchType.SEARCH_UNKNOWN;

    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {
        if (type == SearchType.SEARCH_UNKNOWN) {
            Keyboard keyboard = new ReplyKeyboardMarkup(
                    new String[]{
                            DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH_ARTIST),
                            DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH_MUSIC)},
                    new String[]{
                            DictionaryUtils.getInstance().getResource(Key.BTN_HOME)
                    }
            ).resizeKeyboard(true)
                    .oneTimeKeyboard(true)
                    .selective(true);

            bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.SEARCH))
                    .replyMarkup(keyboard));
        }
    }

    @Override
    public State nextState(String query) {
        if (type == SearchType.SEARCH_UNKNOWN) {
            if (query.equals(DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH_ARTIST))) {
                type = SearchType.SEARCH_ARTIST;
            } else if (query.equals(DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH_MUSIC))) {
                type = SearchType.SEARCH_MUSIC;
            } else if (query.equals(DictionaryUtils.getInstance().getResource(Key.BTN_HOME))) {
                return new HomeState();
            }

            this.nextStateForceExecute = true;
            return this;
        }

        return new InputState(type);
    }

    public SearchType getType() {
        return type;
    }

    public void setType(SearchType type) {
        this.type = type;
    }

    public enum SearchType {
        SEARCH_UNKNOWN(null),
        SEARCH_ARTIST(1),
        SEARCH_MUSIC(2);

        Integer type;

        SearchType(Integer type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return Integer.toString(type);
        }
    }
}
