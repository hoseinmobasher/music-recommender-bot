package state;

import base.ProgressReport;
import base.Result;
import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendAudio;
import com.pengrad.telegrambot.request.SendMessage;
import service.CrawlService;
import utils.DictionaryUtils;
import utils.Key;

import java.util.List;

public class ResultState extends State {
    private SearchState.SearchType type;
    private String query;

    public ResultState(SearchState.SearchType type, String query) {
        super();
        this.type = type;
        this.query = query;

        this.nextStateForceExecute = true;
    }

    public ResultState(SearchState.SearchType type) {
        this.type = type;
    }

    public SearchState.SearchType getType() {
        return type;
    }

    public String getQuery() {
        return query;
    }

    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {
        if (type == SearchState.SearchType.SEARCH_ARTIST) {
            bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.WAITING)));

            List<String> urls = CrawlService.getInstance().crawl(query);

            for (String url : urls) {
                bot.execute(new SendAudio(chatId, url));
            }

            if (urls.size() == 0) {
                bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.NOT_FOUND)));
            }
        }
    }

    @Override
    public State nextState(String query) {
        SearchState state = new SearchState();
        state.setType(SearchState.SearchType.SEARCH_ARTIST);
        state.setNextStateForceExecute(true);

        return state;
    }
}
