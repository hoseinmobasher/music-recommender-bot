package state;

import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.request.SendMessage;
import utils.DictionaryUtils;
import utils.Key;

public class StartState extends State {
    public StartState() {
        super();
        this.nextStateForceExecute = true;
    }

    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {
        bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.WELCOME)));
    }

    @Override
    public State nextState(String query) {
        return new HomeState();
    }
}
