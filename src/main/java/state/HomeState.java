package state;

import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import utils.DictionaryUtils;
import utils.Key;

public class HomeState extends State {
    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {
        Keyboard homeKeyboard = new ReplyKeyboardMarkup(
                new String[]{
                        DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH),
                        DictionaryUtils.getInstance().getResource(Key.BTN_ABOUT)
                }, new String[]{
                        DictionaryUtils.getInstance().getResource(Key.BTN_STOP)
                }
        ).resizeKeyboard(true).selective(true);

        bot.execute(
                new SendMessage(
                        chatId, DictionaryUtils.getInstance().getResource(Key.OPTION)
                ).replyMarkup(homeKeyboard));
    }

    @Override
    public State nextState(String query) {
        if (query.equals(DictionaryUtils.getInstance().getResource(Key.BTN_SEARCH))) {
            return new SearchState();
        } else if (query.equals(DictionaryUtils.getInstance().getResource(Key.BTN_ABOUT))) {
            return new AboutState();
        }

        return new HomeState();
    }
}
