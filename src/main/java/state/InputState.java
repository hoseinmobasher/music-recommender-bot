package state;

import base.State;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.ForceReply;
import com.pengrad.telegrambot.request.SendMessage;
import utils.DictionaryUtils;
import utils.Key;

public class InputState extends State {
    private SearchState.SearchType type;

    public InputState(SearchState.SearchType type) {
        this.type = type;
    }

    public SearchState.SearchType getType() {
        return type;
    }

    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {
        if (type == SearchState.SearchType.SEARCH_ARTIST) {
            bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.ARTIST))
                    .replyMarkup(new ForceReply(true)));
        } else {
            bot.execute(new SendMessage(chatId, DictionaryUtils.getInstance().getResource(Key.NOT_IMPLEMENTED)));
        }
    }

    @Override
    public State nextState(String query) {
        if (type == SearchState.SearchType.SEARCH_ARTIST) {
            return new ResultState(type, query);
        }

        return new HomeState();
    }
}
