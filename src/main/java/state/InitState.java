package state;

import base.State;
import com.pengrad.telegrambot.TelegramBot;

public class InitState extends State {
    public InitState() {
        this.nextStateForceExecute = true;
    }

    @Override
    public void execute(Long chatId, String message, TelegramBot bot) {

    }

    @Override
    public State nextState(String query) {
        return new StartState();
    }
}
