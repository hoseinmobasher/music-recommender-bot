package service;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CrawlService {
    private static final Logger logger = Logger.getLogger(CrawlService.class);
    private static CrawlService instance;

    private WebClient client;
    private String type = "mp3";
    private String urlBase = "https://radiojavan.com";
    private String urlPrefix = "https://host1.rjmediamusic.com/media/";
    private String urlPostfix = ".mp3";

    private CrawlService() {
        client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setJavaScriptEnabled(false);
        client.getOptions().setCssEnabled(false);
    }

    public static CrawlService getInstance() {
        if (instance == null) {
            instance = new CrawlService();
        }

        return instance;
    }

    public List<String> crawl(String artistId) {
        logger.info("Crawl [" + artistId + ", " + type + "]");

        artistId = artistId.replace(' ', '+');

        try {
            HtmlPage page = client.getPage(new URL(String.format("https://www.radiojavan.com/search?query=%s&type=%s", artistId, type)));
            List<HtmlUnorderedList> items = page.getByXPath("//*[@id=\"search_results\"]/div[2]/ul");

            if (items.size() == 0) {
                logger.warn("Nothing found");
                return new ArrayList<>();
            }

            HtmlUnorderedList ul = items.get(0);
            logger.info("ul class:" + ul.getAttribute("class"));

            int size = ul.getChildElementCount();
            int count = 0;

            List<String> urls = new ArrayList<>();
            for (DomElement domElement : ul.getChildElements()) {
                if (count == 10) {
                    return urls;
                }

                count ++;

                HtmlListItem li = (HtmlListItem) domElement;
                HtmlAnchor a = (HtmlAnchor) li.getFirstElementChild();

                String href = a.getHrefAttribute();
                String permalink = urlBase + href;

                HtmlPage trackPage = client.getPage(new URL(permalink));
                List<HtmlAnchor> anchors = trackPage.getByXPath("//*[@id=\"download\"]/a");

                boolean isFound = false;

                if (anchors.size() != 0) {
                    HtmlAnchor anchor = anchors.get(0);
                    String link = anchor.getAttribute("link");

                    if (link.endsWith(".mp3")) {
                        urls.add(link);
                        isFound = true;
                    }
                }

                if (!isFound) {
                    List<HtmlScript> scripts = trackPage.getByXPath("/html/body/script[3]");
                    if (scripts.size() != 0) {
                        HtmlScript script = scripts.get(0);
                        String[] xmlLines = script.asXml().split("\n");

                        String part = "";
                        for (String xmlLine : xmlLines) {
                            xmlLine = xmlLine.trim();

                            if (xmlLine.startsWith("RJ.currentMP3Url")) {
                                part = xmlLine.substring(xmlLine.indexOf('\'') + 1, xmlLine.lastIndexOf('\'')).trim();
                                break;
                            }
                        }

                        if (part.length() != 0) {
                            urls.add(urlPrefix + part + urlPostfix);
                            isFound = true;
                        }
                    }
                }

                if (!isFound) {
                    href = urlPrefix + "mp3/mp3/" + href.substring(href.lastIndexOf('/') + 1) + urlPostfix;
                    urls.add(href);
                }
            }

            return urls;
        } catch (IOException e) {
            logger.error("Exception in getting page", e);
        }

        return new ArrayList<>();
    }
}
