package base;

public interface ProgressReport<T> {
    void publish(T progress);
}
