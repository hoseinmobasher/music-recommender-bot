package base;

import java.util.List;

public interface Result<T> {
    void onSuccess(List<T> results);
    void onFailure(String error);
}
