package base;

import com.pengrad.telegrambot.TelegramBot;

public abstract class State {
    protected boolean nextStateForceExecute;

    public State() {
        this.nextStateForceExecute = false;
    }

    public boolean isNextStateForceExecute() {
        return nextStateForceExecute;
    }

    public void setNextStateForceExecute(boolean nextStateForceExecute) {
        this.nextStateForceExecute = nextStateForceExecute;
    }

    public abstract void execute(Long chatId, String message, TelegramBot bot);
    public abstract State nextState(String query);
}
