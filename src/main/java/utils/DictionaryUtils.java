package utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DictionaryUtils {
    private static Logger logger = Logger.getLogger(DictionaryUtils.class);
    private static DictionaryUtils instance;

    private Map<Key, String> properties;

    public static DictionaryUtils getInstance() {
        if (instance == null) {
            instance = new DictionaryUtils();
        }

        return instance;
    }

    private DictionaryUtils() {
        init();
    }

    private void init() {
        if (properties == null) {
            properties = new HashMap<>();
        }

        InputStream is = null;
        InputStreamReader reader = null;
        Properties prop = new Properties();

        try {
            is = getClass().getResourceAsStream("/dictionary.properties");
            reader = new InputStreamReader(is,"UTF-8");

            prop.load(reader);

            for (Key key: Key.values()) {
                properties.put(key, prop.getProperty(key.getKey(), key.getDefaultValue()));
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }

            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    public String getResource(Key key) {
        if (properties.containsKey(key)) {
            return properties.get(key);
        }

        return "";
    }
}
