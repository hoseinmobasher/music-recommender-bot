package utils;

public enum Key {
    WELCOME("welcome", "", true),
    ABOUT("about", "", true),
    UNKNOWN("unknown", "", true),
    NOT_IMPLEMENTED("not_implemented", "", true),
    SEARCH("search", "", true),
    OPTION("option", "", true),
    MUSIC("music", "", true),
    ARTIST("artist", "", true),
    MIX_SEARCH("mix_search", "", true),

    WAITING("waiting", "", true),
    NOT_FOUND("not_found", "", true),

    BTN_SEARCH("btn_search", "", true),
    BTN_ABOUT("btn_about", "", true),
    BTN_STOP("btn_stop", "", true),

    BTN_SEARCH_ARTIST("btn_search_artist", "", true),
    BTN_SEARCH_MUSIC("btn_search_music", "", true),
    BTN_HOME("btn_home", "", true),

    BTN_MORE_RESULT("btn_more_result", "", true);

    private String key;
    private String defaultValue;
    private boolean enabled;

    Key(String key, String defaultValue, boolean enabled) {
        this.key = key;
        this.defaultValue = defaultValue;
        this.enabled = enabled;
    }

    public String getKey() {
        return key;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return this.key;
    }
}
